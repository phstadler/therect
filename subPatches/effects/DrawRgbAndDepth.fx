//texture
texture Tex <string uiname="Texture";>;  // used for depth data
texture Tex2 <string uiname="Texture2";>; // used for rgb data

sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler Samp2 = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex2);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};
 
//the data structure: "vertexshader to pixelshader"
//used as output data with the VS function
//and as input data with the PS function
struct vs2ps
{
    float4 Pos  : POSITION;
    float2 TexCd : TEXCOORD0;
	float2 TexCD2 : TEXCOORD0;
};


float4 ConvertToGray(float4 col)
{
    const float4 lumcoeff = {0.3, 0.59, 0.11, 0};
    return dot(col, lumcoeff);
}

float GetScaledRed(float inRed) {
	
}

float2 PixelSize;
float ThresholdNear = 0;
float ThresholdFar = 1;
float4 PS(vs2ps In): COLOR
{
	
	float brightness = 5;
	
	float4 col = tex2D(Samp, In.TexCd);
	float4 col2 = tex2D(Samp2, In.TexCD2);
	//col.rgb = 1 - col.rgb;
	col *= brightness;
	//col.g = col.b = 0;
	
	if (col.r > ThresholdNear && col.r < ThresholdFar) {
		//col.r = col.r / 10;// * 100;
		
		col.g = col.r + col2.g;
		col.b = col.r + col2.b;
		
		if (col.g > 0.4) {
			col.g *= 3;
		}
		if (col.b > 0.4) {
			col.b *= 3;
		}
		//col.g = col2.g; //rgb
		//col.b = col2.b; //rgb
		col.r /= 2;
	} else {
		col.g = col.b = col.r = 0;
	}
	
	return col;
}

 
technique TSimpleShader
{
    pass P0
    {
        VertexShader = null;
        PixelShader  = compile ps_2_0 PS();
    }
}