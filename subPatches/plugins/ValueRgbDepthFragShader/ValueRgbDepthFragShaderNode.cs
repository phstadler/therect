//texture
texture Tex <string uiname="Texture";>;

sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};
 
//the data structure: "vertexshader to pixelshader"
//used as output data with the VS function
//and as input data with the PS function
struct vs2ps
{
    float4 Pos  : POSITION;
    float2 TexCd : TEXCOORD0;
};


float4 ConvertToGray(float4 col)
{
    const float4 lumcoeff = {0.3, 0.59, 0.11, 0};
    return dot(col, lumcoeff);
}

float GetScaledRed(float inRed) {
	
}

float2 PixelSize;
float Threshold = 0.2;
float4 PS(vs2ps In): COLOR
{
	
	float brightness = 10;
	
	float4 col = tex2D(Samp, In.TexCd);
	//col.rgb = 1 - col.rgb;
	col *= brightness;
	
	if (col.r > .01 && col.r < 0.25) {
		col.r = 1;
	}
	
	return col;
	

	
    //the texture coordinate offset with vertical coordinate set to 0
    float2 off = float2(PixelSize.x, 0);
 
    //sample the left and the right neighbouring pixels
    float4 left = tex2D(Samp, In.TexCd - off);
	left *= brightness;
    float4 right = tex2D(Samp, In.TexCd + off);
	right *= brightness;
	
	
 
    if (abs(ConvertToGray(left).x - ConvertToGray(right).x) > Threshold)
      return 1;
    else
      return float4(0, 0, 0, 1);
}

 
technique TSimpleShader
{
    pass P0
    {
        VertexShader = null;
        PixelShader  = compile ps_2_0 PS();
    }
}