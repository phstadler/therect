ASynth 1.01 demo

Copyright 2004-2005 Antti Huovilainen
All Rights Reserved

http://antti.smartelectronix.com

Credits:
Programming by Antti @ Smartelectronix.com
Graphics by George R. Reales
Presets by ToTc Productions
VST Plug-In Technology by Steinberg

Features:
6 Voice polyphony
2x/4x/8x selectable oversampling
Midi CC automation
Two oscillators:
  Osc 1 with sawtooth and square waveforms
  Osc 2 with sawtooth and pulse waveforms
Three circuit modelled filters:
  MS20: 12 dB/oct multimode filter with signal level dependent resonance.
  Mini: 24 dB/oct filter from the 60's with added multimode responses.
  XPander: Multimode 24 dB/oct filter with 16 different responses.
Two ADSR envelopes (VCF and VCA)
One LFO with 6 waveforms

Version history:
1.0     Initial release

1.01    Fixes:
          A severe bug in note timing was fixed
          Limited max cutoff to ~30kHz to remove artifacts when using 
            very high cutoff and envelope amount
        New Features:
          Added XPander modes to Mini filter too
          Added bandpass and highpass mode to MS20 filter
                        
A word about the filters:
The filters in ASynth are all emulations of the actual analog filter circuits 
with the exception of XPander which is based on OTA (Operational 
Transconductance Amplifier) core. Hence they are slower than normal digital 
filters, but the difference in sound should more than make up for the
increased cpu requirements. For more details on the technology, see

  Antti Huovilainen, "Non-linear Digital Implementation of the Moog Ladder 
  Filter", In Proceedings of the 7th International Conference on Digital Audio 
  Effects (DAFx-04), Naples, Italy, Oct. 2004.

Registration: 
This demo version is fully functional but has a nagscreen on startup. To 
register (and get rid of the nagscreen), go to http://antti.smartelectronix.com
and click "Donate". You are free to choose the amount of the donation yourself 
depending on how useful you find ASynth. While the only immediate benefit of
registration is removal of the nagscreen, I am more likely to continue
development if people donate and your feature requests are more likely to be
listened to than some Joe Random user's.

Installation:
Unpack the files to your VST plugins folder (or some subfolder).
ASynthCC.txt should be placed in same directory with ASynth.dll.
This will set global (for all users) CC mapping. The plugin will 
automatically create an empty ASynthCC.txt to user's
"Application Data\Smartelectronix" directory on first startup. Any settings
entered here are user specific and will override the settings in global 
ASynthCC.txt. The syntax is documented in the beginning of the file. To 
recreate the documentation, simply remove the user specific config file and
an empty one will be created.

Controls:

LFO:
  Wave:   Selects LFO waveform. Choices are sine, triangle, rising sawtooth,
          falling sawtooth, square and sample&hold.
  Speed:  Sets LFO speed. The speed ranges from 0.02 Hz (50 secs) to 20 Hz
          (50 msecs).
  Amount: Sets LFO amount. The range is from 0 to 1 (+- 1 octave for 
          oscillators and filter and +- 50% for oscillator 2 PWM).
  Wheel:  Switch LFO amount to be controlled by modulation wheel. The
          effective amount is LFO Amount * Mod Wheel (0 .. 1).
  Pitch:  Applies LFO to both oscillators' pitch
  PW:     Applies LFO to oscillator 2 pulse-width
  Filter: Applies LFO to filter cutoff

Oscillators:
  Osc1 Wave:  Selects oscillator 1 waveform. Choices are sawtooth and square
  Mix:        Selects the mixing relation between the oscillators (0% means 
              osc1 is at full level and osc2 is off).
  Semi:       Sets oscillator 2 detune in semitones (-12 .. +12).
  Cents:      Sets oscillator 2 detune in cents (0..20).
  Lin:        Sets oscillator 2 linear (independent of note) detune in Hz 
              (-1 Hz .. +1 Hz).
  Osc2 Wave:  Selects oscillator 2 waveform. Choices are sawtooth and pulse.
  PW:         Sets oscillator 2 pulse-width.
  
Filter:
  Drive:  Sets filter drive. Higher drive means more distortion and lower 
          resonance amplitude.
  Cutoff: Sets filter cutoff (10 Hz .. 10240 Hz).
  Reso:   Sets filter resonance (0 .. 1.5). Filter starts to self-oscillate
          at 1.0.
  Keytrk: Sets filter keyboard tracking amount (0 .. 1). Setting of zero
          means the filter is not affected by note, while setting of one
          means that the filter frequency is directly dependant on note
          (moving up one octave in keyboard doubles the filter cutoff).
  Type:   Selects filter type between MS20 (12 dB), Mini (24 dB) and XPander
          (24 dB multimode).
  Mode:   Selects response for the filter. Available responses are
  
          Mini & XPander:
            4P BP (12dB+12dB bandpass), 3PHP+1PLP (18dB highpass + 6dB lowpass),
            3PAP+1PLP (allpass + 6dB lowpass), 2PN+1PLP (Notch + 6dB lowpass),
            2PLP (12dB lowpass), 2PHP+1PLP (12dB highpass + 6dB lowpass),
            4PLP (24dB lowpass), 2PBP (6dB+6dB bandpass).
            In the following modes the resonance frequency is doubled. 
            2PHP+1PLP (12dB highpass + 6dB lowpass), 3PHP (18dB highpass),
            3PAP (allpass), 2PN (notch), 1P LP (6dB lowpass),
            2PHP (12dB highpass), 3PLP (18dB lowpass), 1PHP (6dB highpass).
          MS20:
            Lowpass, Bandpass, Highpass
          
Filter envelope: (ADSR)
  Attack:   Sets time taken for envelope to reach maximum from note on
            (2msecs - 2secs).
  Decay:    Sets time taken for envelope to reach two thirds to sustain level
            from maximum level.
  Sustain:  Sets sustain level.
  Release:  Sets time taken for envelope to decay to one third of sustain
            level once note is released.
  Env:      Sets how much filter envelope affects filter cutoff frequency 
            (0 .. 7 octaves).

Amplifier envelope:
  Same as filter envelope but no env amount control.

Global:
  Glide:        Sets time taken to reach within one third of next note in
                monophonic mode (ignored in polyphonic mode).
  Output:       Sets output level.
  Poly:         Selects between monophonic and polyphonic mode.
  Oversampling: Selects between 8x/4x/2x oversampling. More oversampling gives 
                better quality but also consumes more cpu power. 2x oversampling 
                is enough for many sounds and 4x for most of the rest. If lots 
                of drive is used with resonance over 1.0, the oversampling 
                should be increased (especially for MS20 and XPander filters). 
                Additionally, MS20 filter maximum effective cutoff frequency is 
                only 11 KHz using 2x oversampling.



Hope you enjoy the synth - Antti
