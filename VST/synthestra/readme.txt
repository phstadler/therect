MARVIN VST
==========

Monophonic Synthesizer - Synthestra Version 1.0

	This is a freeware for Windows.
	There is no warranty.


Features
--------
	Made with SynthEdit

	2 oscillators & Noise Generator
		- VCO1: Saw & Pulse (PWM), Sub OSC (Sine/Square)
		- VCO2: Saw & Pulse (PWM)
		- White Noise
	LFO
		- Waveform (Triangle/Square), Rate, Delay, Depth (VCO/VCF/VCA)
	LPF
		- Cutoff, Resonance, Envelope, Key-follow, Velocity
	VCA
		- Envelope (Attack, Decay,Sustain, Release)/Gate, Main Volume
	MIDI CC support
	Automation support


MIDI CC
-------

	#3 Retrigger
	#9 Bender
	#14 Mod Wheel
	#5 Glide
	#7 Volume

	#15 LFO Waveform
	#16 LFO Rate
	#17 LFO Delay
	#18 LFO Depth Type
	#19 LFO Depth

	#21 VCO1 Octave
	#22 VCO1 Waveform
	#23 VCO1 PWM Type
	#24 VCO1 PWM
	#25 VCO1 Sub Wave
	#26 VCO1 Sub Octave
	#71 VCO2 Octave
	#72 VCO2 Waveform
	#73 VCO2 PWM Type
	#74 VCO2 PWM
	#75 VCO2 Interval
	#76 VCO2 Fine
	#77 VCO Detune

	#78 Mixer VCO1
	#79 Mixer SubOSC
	#80 Mixer VCO2
	#81 Mixer Noise

	#82 VCF Cutoff
	#83 VCF Resonance
	#84 VCF Envelope Type
	#85 VCF Envelope
	#86 VCF Key Follow
	#87 VCF Velocity

	#88 VCA Type
	#89 VCA Velocity
	#90 VCA Attack
	#91 VCA Decay
	#92 VCA Sustain
	#93 VCA Release


-----------------------------------------
Author  : Runa Suzuki
E-mail  : marvin.runa@gmail.com
Website : http://marvinpavilion.ojaru.jp/
-----------------------------------------
Copyright (C) 2011 MARVIN All Rights Reserved.