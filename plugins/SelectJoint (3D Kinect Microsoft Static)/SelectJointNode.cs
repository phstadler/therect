#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	//create your own enum type or use any other .net enum
	public enum KinectJoints
	{
		HipCenter,
        Spine,
        ShoulderCenter,
        Head,
        ShoulderLeft,
        ElbowLeft,
        WristLeft,
        HandLeft,
        ShoulderRight,
        ElbowRight,
        WristRight,
        HandRight,
        HipLeft,
        KneeLeft,
        AnkleLeft,
        FootLeft,
        HipRight,
        KneeRight,
        AnkleRight,
        FootRight,
	}
	//Please rename your Enum Type to avoid 
	//numerous "MyEnum"s in the system

	#region PluginInfo
	[PluginInfo(Name = "SelectJoint", Category = "3D Kinect Microsoft", Version = "Static", Help = "Select Joint by enum for Kinect Microsoft", Tags = "Select, Joint")]
	#endregion PluginInfo
	public class SelectJointNode : IPluginEvaluate
	{
		#region fields & pins

		[Input("Input")]
		ISpread<Vector3D> FXYZInput;
		
		[Input("Select", DefaultEnumEntry = "HipCenter")]
		IDiffSpread<KinectJoints> FInput;
				
		[Output("Output")]
		ISpread<Vector3D> FXYZOutput;
		
		[Output("Index")]
		ISpread<int> FOrdOutput;
		
		[Output("User")]
		ISpread<int> FUserOutput;
		
		[Import()]
		ILogger Flogger;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			
			int sUsers = (FXYZInput.SliceCount/20);
			int sJoints = FInput.SliceCount;
			int sMax = sUsers * sJoints;

   			FXYZOutput.SliceCount = sMax;
   			FOrdOutput.SliceCount = sMax;
  			FUserOutput.SliceCount = sMax;
			
  			int cnt = 0;
  			for (int i = 0; i < sUsers; i++)
   			{
    			int startidx = i * 20;
    			for (int j = 0 ; j < sJoints; j++)
    			{
     				int jid = (int)FInput[j];
     				FXYZOutput[cnt] = FXYZInput[jid+startidx];
     				FOrdOutput[cnt] = jid + startidx;
     				FUserOutput[cnt] = i;
    				cnt++;
    			}
   			}
		}
	}
}